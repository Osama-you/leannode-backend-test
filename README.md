# Simple Backend Project #

Create simple APIs for user management which uses to following instructions:

 - Create new project using SailsJs
 - Store data in Firestore database using firebase admin SDK
 - Generate and verify tokens by JTW using jsonwebtoken library
 - Create postman collection to test your APIs
 - Create Github or Bitbucket repository and push your code

## Getting Started 📣
**1. You can start by cloning the repository on your local machine by running:**

```sh
git clone https://Osama-you@bitbucket.org/Osama-you/leannode-backend-test.git
cd leannode-backend-test
```

**2. Environment variables:🔑**
- create ./.env file
- add your Environment variables
    ```sh
    SECRET=

    FB_TYPE=
    FB_PROJECT_ID=
    FB_PRIVATE_KEY_ID=
    FB_PRIVATE_KEY=
    FB_CLIENT_EMAIL=
    FB_CLIENT_ID=
    FB_AUTH_URI=
    FB_TOKEN_URI=
    FB_AUTH_PROVIDER_X509_CERT_URL=
    FB_CLIENT_X509_CERT_URL

    FB_DATABASE_URL=
    ```
    **Note: There is a problem that occurs when you FB_PRIVATE_KEY in the env file**
   So as a temporary solution, you can go [this path](https://bitbucket.org/Osama-you/leannode-backend-test/src/ef5a6af90b2196e77797fbceedda6adac4081748/config/env/firebase.js#lines-37) and add it there
    
**3. run the app locally:🔌**

```
yarn dev
```

Now the app should be running at [http://localhost:1337](http://localhost:1337)