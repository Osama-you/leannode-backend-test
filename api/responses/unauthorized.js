module.exports = function unauthorized() {
  const res = this.res;

  return res.sendStatus(401);
};
