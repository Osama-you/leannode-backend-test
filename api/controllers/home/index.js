const HomeController = {
  friendlyName: 'Index',
  description: 'Index home.',
  inputs: {},
  exits: {
    success: {
      description: 'Api is working.',
    },
  },
  fn: async function () {
    this.res.json({ msg: 'Hello From Api' });
  },
};

module.exports = HomeController;
