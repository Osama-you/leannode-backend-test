const { compare } = require('bcrypt');

const { createToken } = require('../../../utils');

const LoginController = {
  friendlyName: 'AdminLogin',
  description:
    'Admin Log in using the provided username and password combination.',
  inputs: {
    username: {
      required: true,
      type: 'string',
      description: 'The username for the new account, e.g. username2.',
      extendedDescription: 'Must be a valid username.',
    },
    password: {
      required: true,
      type: 'string',
      maxLength: 200,
      example: 'password-lol',
      description: 'The unencrypted password to use for the new account.',
      custom: function (value) {
        return (
          _.isString(value) &&
          value.length >= 6 &&
          value.match(/[a-z]/i) &&
          value.match(/[0-9]/)
        );
      },
    },
  },

  fn: async function () {
    const req = this.req;
    const res = this.res;

    const { username, password } = req.body;
    const { findUserByUsername } = FirebaseService;
    try {
      const userByUsername = await findUserByUsername({
        username,
      });
      if (!userByUsername) {
        return res.badRequest({
          message: 'The username or/and password is invalid',
        });
      }
      const userId = Object.keys(userByUsername)[0];

      if (!userByUsername[userId].isAdmin) {
        return res.unauthorized();
      }

      const isPasswordValid = await compare(
        password,
        userByUsername[userId].password
      );
      if (!isPasswordValid) {
        return res.badRequest({
          message: 'The username or/and password is invalid',
        });
      }

      const { token, tokenName, options } = createToken({
        id: userId,
      });

      return res
        .cookie(tokenName, token, options)
        .ok({ message: 'Welcome back, Admin ^_^ !' });
    } catch ({ message }) {
      return res.serverError({ message });
    }
  },
};

module.exports = LoginController;
