const GetUsersController = {
  friendlyName: 'GetUsers',
  description: 'Admin endPoint using for get all users.',
  fn: async function () {
    const req = this.req;
    const res = this.res;

    const { limit } = req.query;
    try {
      const UsersAsObj = await FirebaseService.getAllUser({
        limit: parseInt(limit) || 10,
      });

      const UsersAsArray = Object.entries(UsersAsObj).map(
        ([id, { username, isAdmin, email, age }]) => ({
          id,
          username,
          email,
          age,
          isAdmin,
        })
      );

      res.ok({ users: UsersAsArray });
    } catch ({ message }) {
      return res.serverError({ message });
    }
  },
};

module.exports = GetUsersController;
