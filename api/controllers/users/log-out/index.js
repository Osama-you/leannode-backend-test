const { TOKEN_NAME } = require('../../../constants');

const logoutController = {
  friendlyName: 'Logout',
  description: 'Log out of this app.',
  fn: async function () {
    const res = this.res;

    res.clearCookie(TOKEN_NAME).ok();
  },
};

module.exports = logoutController;
