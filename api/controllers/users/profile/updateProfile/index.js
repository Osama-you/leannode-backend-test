const UpdateProfileController = {
  friendlyName: 'Update Profile',
  description: 'Update Profile data for login user.',
  inputs: {
    username: {
      type: 'string',
      description: 'The username for the new account, e.g. username2.',
      extendedDescription: 'Must be a valid username.',
    },
    email: {
      type: 'string',
      isEmail: true,
      description: 'The email address for the new account, e.g. m@example.com.',
      extendedDescription: 'Must be a valid email address.',
    },
    password: {
      type: 'string',
      maxLength: 200,
      example: 'password-lol',
      description: 'The unencrypted password to use for the new account.',
      custom: function (value) {
        return (
          _.isString(value) &&
          value.length >= 6 &&
          value.match(/[a-z]/i) &&
          value.match(/[0-9]/)
        );
      },
    },
    age: {
      type: 'number',
      example: 20,
      description: 'The user Age.',
      min: 18,
      max: 50,
    },
  },
  fn: async function () {
    const req = this.req;
    const res = this.res;
    const {
      user: { id },
    } = req;

    const { username, email, password, age } = req.body;

    const { findUserByUsername, findUserByEmail, updateUserById } =
      FirebaseService;
    try {
      const newData = {};

      // If there is username
      if (username) {
        const userByUsername = await findUserByUsername({
          username,
        });
        if (userByUsername) {
          return res.badRequest({ message: 'The username is invalid' });
        }
        newData.username = username;
      }

      // If there is email
      if (email) {
        const userByEmail = await findUserByEmail({
          email,
        });
        if (userByEmail) {
          return res.badRequest({ message: 'The email is invalid' });
        }
        newData.email = email;
      }

      // If there is password
      if (password) {
        const hashedPassword = await hash(password, 10);
        newData.password = hashedPassword;
      }

      // If there is age
      if (age) {
        newData.age = age;
      }

      if (Object.keys(newData).length === 0) {
        return res.ok({ message: 'Nothing to updated' });
      }

      await updateUserById(id, newData);

      return res.ok({ message: 'User data updated successfully' });
    } catch ({ message }) {
      return res.serverError({ message });
    }
  },
};

module.exports = UpdateProfileController;
