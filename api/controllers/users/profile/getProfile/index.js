const GetProfileController = {
  friendlyName: 'User Profile',
  description: 'Profile data for login user.',
  fn: async function () {
    const { user } = this.req;
    const res = this.res;

    delete user.password;
    res.json({ user });
  },
};

module.exports = GetProfileController;
