const { hash } = require('bcrypt');

const { createToken } = require('../../../utils');

const SignupController = {
  friendlyName: 'Signup',

  description: 'Sign up for a new user account.',

  inputs: {
    username: {
      required: true,
      type: 'string',
      description: 'The username for the new account, e.g. username2.',
      extendedDescription: 'Must be a valid username.',
    },
    email: {
      required: true,
      type: 'string',
      isEmail: true,
      description: 'The email address for the new account, e.g. m@example.com.',
      extendedDescription: 'Must be a valid email address.',
    },
    password: {
      required: true,
      type: 'string',
      maxLength: 200,
      example: 'password-lol',
      description: 'The unencrypted password to use for the new account.',
      custom: function (value) {
        return (
          _.isString(value) &&
          value.length >= 6 &&
          value.match(/[a-z]/i) &&
          value.match(/[0-9]/)
        );
      },
    },
    age: {
      required: true,
      type: 'number',
      example: 20,
      description: 'The user Age.',
      min: 18,
      max: 50,
    },
  },
  fn: async function () {
    const req = this.req;
    const res = this.res;
    const { email, password, username, age } = req.body;

    const { findUserByUsername, findUserByEmail, createUser } = FirebaseService;
    try {
      const userByEmail = await findUserByEmail({
        email,
      });
      if (userByEmail) {
        return res.badRequest({ message: 'The email is already exist' });
      }

      const userByUsername = await findUserByUsername({
        username,
      });
      if (userByUsername) {
        return res.badRequest({ message: 'The username is already exist' });
      }

      const hashedPassword = await hash(password, 10);
      const user = await createUser({
        username,
        email,
        password: hashedPassword,
        age,
      });
      const { token, tokenName, options } = createToken({
        id: user.id,
      });

      return res
        .cookie(tokenName, token, options)
        .status(201)
        .json({ message: 'Signed up successfully' });
    } catch ({ message }) {
      return res.serverError({ message });
    }
  },
};

module.exports = SignupController;
