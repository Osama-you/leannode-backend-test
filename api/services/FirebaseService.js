const admin = require('firebase-admin');
const config = require('../../config/env');

const serviceAccount = JSON.parse(
  JSON.stringify(config.firebase.serviceAccount)
);

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: config.firebase.databaseURL,
});

const db = admin.database();

const ref = {
  users: db.ref('/users'),
};

const findUserByEmail = async ({ email }) => {
  const snapshot = await ref.users
    .orderByChild('email')
    .equalTo(email)
    .once('value');
  return snapshot.val();
};

const findUserByUsername = async ({ username }) => {
  const snapshot = await ref.users
    .orderByChild('username')
    .equalTo(username)
    .once('value');
  return snapshot.val();
};

const createUser = async ({
  username,
  email,
  password,
  age,
  isAdmin = false,
}) => {
  const userId = await ref.users.push().key;
  ref.users.child(userId).set({ username, email, password, age, isAdmin });
  return { id: userId };
};

const findUserById = async ({ id }) => {
  const data = await ref.users.child(id).once('value');
  return data.val();
};

const updateUserById = (id, data) => {
  ref.users.child(id).update(data);
};

const getAllUser = async ({ limit }) => {
  const data = await ref.users.limitToFirst(limit).once('value');
  return data.val();
};

module.exports = {
  db,
  findUserByEmail,
  findUserByUsername,
  createUser,
  findUserById,
  updateUserById,
  getAllUser,
};
