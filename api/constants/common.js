const TOKEN_NAME = 'token';

const TOKEN_MAX_AGE = 365 * 24 * 60 * 60 * 1000;

const envTypes = {
  PRODUCTION: 'production',
  DEVELOPMENT: 'development',
  TEST: 'test',
};

module.exports = { TOKEN_NAME, TOKEN_MAX_AGE, envTypes };
