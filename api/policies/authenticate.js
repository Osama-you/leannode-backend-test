const { verify } = require('jsonwebtoken');

const { TOKEN_NAME } = require('../constants');
const config = require('../../config/env');

const { secret } = config.server;

module.exports = async function (req, res, next) {
  const { findUserById } = FirebaseService;
  try {
    const { cookies } = req;
    if (!cookies || !cookies[TOKEN_NAME]) {
      return res.unauthorized();
    }

    let decoded;
    try {
      // verify the token
      decoded = await verify(cookies[TOKEN_NAME], secret);
      // if not valid clear token
    } catch (error) {
      res.clearCookie(TOKEN_NAME);
      return res.unauthorized();
    }

    // get user Id from token
    const { id } = decoded;
    const user = await findUserById({ id });

    if (!user) {
      res.clearCookie(TOKEN_NAME);
      return res.unauthorized();
    }
    user['id'] = id;

    // put the user info in the req to be accessed in the next middleware
    req.user = user;
    return next();
  } catch ({ message }) {
    return res.serverError({ message });
  }
};
