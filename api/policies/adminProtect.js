module.exports = async function (req, res, next) {
  const { isAdmin } = req.user;
  try {
    if (!isAdmin) {
      return res.unauthorized();
    }
    return next();
  } catch ({ message }) {
    return res.serverError({ message });
  }
};
