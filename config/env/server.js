const yup = require('yup');

const envVarsSchema = yup
  .object()
  .shape({
    SECRET: yup.string().required(),
  })
  .required();

const config = () => {
  let envVars;
  try {
    envVars = envVarsSchema.validateSync(process.env, { stripUnknown: true });
  } catch (error) {
    if (error) {
      throw new Error(`Config validation error: ${error.message}`);
    }
  }

  return {
    secret: envVars.SECRET,
  };
};

module.exports = config;
