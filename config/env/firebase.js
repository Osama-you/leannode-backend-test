/* eslint-disable camelcase */
const yup = require('yup');

const envVarsSchema = yup
  .object()
  .shape({
    FB_TYPE: yup.string().required(),
    FB_PROJECT_ID: yup.string().required(),
    FB_PRIVATE_KEY_ID: yup.string().required(),
    // FB_PRIVATE_KEY: yup.string().required(),
    FB_CLIENT_EMAIL: yup.string().required(),
    FB_CLIENT_ID: yup.string().required(),
    FB_AUTH_URI: yup.string().required(),
    FB_TOKEN_URI: yup.string().required(),
    FB_AUTH_PROVIDER_X509_CERT_URL: yup.string().required(),
    FB_CLIENT_X509_CERT_URL: yup.string().required(),
    FB_DATABASE_URL: yup.string().required(),
  })
  .required();

const config = () => {
  let envVars;
  try {
    envVars = envVarsSchema.validateSync(process.env, { stripUnknown: true });
  } catch (error) {
    if (error) {
      throw new Error(`Config validation error: ${error.message}`);
    }
  }

  return {
    serviceAccount: {
      type: envVars.FB_TYPE,
      project_id: envVars.FB_PROJECT_ID,
      private_key_id: envVars.FB_PRIVATE_KEY_ID,
      // private_key: envVars.FB_PRIVATE_KEY,   // There is error when I stor it in the env
      private_key: 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
      client_email: envVars.FB_CLIENT_EMAIL,
      client_id: envVars.FB_CLIENT_ID,
      auth_uri: envVars.FB_AUTH_URI,
      token_uri: envVars.FB_TOKEN_URI,
      auth_provider_x509_cert_url: envVars.FB_AUTH_PROVIDER_X509_CERT_URL,
      client_x509_cert_url: envVars.FB_CLIENT_X509_CERT_URL,
    },
    databaseURL: envVars.FB_DATABASE_URL,
  };
};

module.exports = config;
