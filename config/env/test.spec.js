const { expect } = require('chai');
const config = require('./index');

describe('validate config', () => {
  it('should return the required env variables', () => {
    expect(config).to.have.property('common');
    expect(config).to.have.property('server');
    expect(config).to.have.property('production');
  });

  it('common config', () => {
    expect(config.common).to.have.property('env');
  });

  it('server config', () => {
    expect(config.server).to.have.property('secret');
  });

  it('firebase config', () => {
    const {
      firebase: { serviceAccount },
    } = config;
    expect(serviceAccount).to.have.property('type');
    expect(serviceAccount).to.have.property('project_id');
    expect(serviceAccount).to.have.property('private_key_id');
    // expect(serviceAccount).to.have.property('private_key');
    expect(serviceAccount).to.have.property('client_email');
    expect(serviceAccount).to.have.property('client_id');
    expect(serviceAccount).to.have.property('token_uri');
    expect(serviceAccount).to.have.property('auth_provider_x509_cert_url');
    expect(serviceAccount).to.have.property('client_x509_cert_url');
    expect(config.firebase).to.have.property('databaseURL');
  });
});
