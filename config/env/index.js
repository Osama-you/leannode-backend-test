const dotenv = require('dotenv');
const common = require('./common');
const server = require('./server');
const firebase = require('./firebase');
const production = require('./production');

if (process.env.NODE_ENV !== 'production') {
  dotenv.config();
}

try {
  common();
  server();
  firebase();
} catch (error) {
  throw new Error(`Error in config validation: ${error.message}`);
}

module.exports = {
  common: common(),
  server: server(),
  firebase: firebase(),
  production,
};
