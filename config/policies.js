module.exports.policies = {
  'users/profile/*': 'authenticate',
  'admin/getUsers/*': ['authenticate', 'adminProtect'],
  // '*': true,
};
