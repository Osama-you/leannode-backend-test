module.exports.routes = {
  '/api/v1/': 'home/index',
  'POST /api/v1/sign-up': 'users/sign-up/index',
  'POST /api/v1/log-out': 'users/log-out/index',
  'POST /api/v1/log-in': 'users/log-in/index',
  '/api/v1/profile': 'users/profile/getProfile/index',
  'PATCH /api/v1/profile': 'users/profile/updateProfile/index',
  'POST /api/v1/admin/login': 'admin/login/index',
  '/api/v1/admin/users': 'admin/getUsers/index',
};
